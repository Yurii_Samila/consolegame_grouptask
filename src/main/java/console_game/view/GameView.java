package console_game.view;

import console_game.model.entity.Card;
import console_game.model.entity.Game;
import java.util.List;

public class GameView {
  public void printWelcome(){
    System.out.println("Welcome to the best console game!!!");
  }

  public void printGameField(Game game){
    System.out.println(
          "1) Put the card\n"
        + "2) Attack the enemy card\n"
        + "3) Attack the enemy hero\n"
        + "4) End turn");
  }

  public void printCardsOnField(List<Card> cardsOnField){

  }

  public void chooseEnemyCard(){
    System.out.println("Please, choose enemy card you want to attack");
  }

  public void noAvailableCardsWarning(){
    System.out.println("You don't have any card to use");
  }

  public void printCards(List<Card> cards){
    System.out.println("Enter number of the card");
    for (int i = 0; i < cards.size(); i++) {
      System.out.println(i+1 + " - " + cards.get(i));

    }
  }

  public void chooseTheEnemy(){
    System.out.println("If you don't have any friends and you want to play with stupid machine "
        + "without soul - tap '1',"
        + " otherwise - tap any key");
  }
  public void errorMessage(){
    System.out.println("OMG!!!!Could you please tap number from '1' to '4'???");
  }
}
