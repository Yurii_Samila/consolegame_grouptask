package console_game.model.entity;

import console_game.model.entity.entityService.Randomizer;
import java.util.List;

public class Game {

  private Hero player;
  private Hero enemy;
  private final int START_NUM_CARDS = 4;
  private Randomizer randomizer;

  public Game() {
    this.randomizer = new Randomizer();
    this.player = initHero();
    this.enemy = initHero();
  }

  public Hero getAttacker(){
    return player.isHisTurn() ? player : enemy;
  }
  public Hero getDefender(){
    return !player.isHisTurn() ? player : enemy;
  }

  private Hero initHero() {
    Hero currentHero = new Hero();
    List<Card> heroCards = currentHero.getCardsOnHand();
    for (int i = 0; i < START_NUM_CARDS; i++) {
      heroCards.add(randomizer.getRandomCard());
    }
    currentHero.setCardsOnHand(heroCards);
    return currentHero;
  }

  public Hero getPlayer() {
    return player;
  }

  public void setPlayer(Hero player) {
    this.player = player;
  }

  public Hero getEnemy() {
    return enemy;
  }

  public void setEnemy(Hero enemy) {
    this.enemy = enemy;
  }

}
