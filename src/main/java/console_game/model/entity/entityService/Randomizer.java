package console_game.model.entity.entityService;

import console_game.model.entity.Card;

public class Randomizer {
  public int getRandomNum(int maxNum){
    if (maxNum==0){
      return 0;
    }
    return (int)(Math.round(Math.random() * (maxNum -1)));
  }
  public Card getRandomCard(){
    int cardNumber = getRandomNum(Card.values().length);
    return Card.values()[cardNumber];
  }

}
