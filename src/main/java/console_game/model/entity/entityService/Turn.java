package console_game.model.entity.entityService;

import console_game.model.entity.Card;
import console_game.model.entity.Game;
import console_game.model.entity.Hero;
import java.util.ArrayList;
import java.util.List;

public class Turn  {

  private Game game;
  private Randomizer randomizer;
  public Turn(Game game) {
    this.game = game;
    this.randomizer = new Randomizer();
  }

  public void cardAttack(int attackingCardIndex, int defendingCardIndex) {
    Hero heroAttacker = game.getAttacker();
    Hero heroDefender = game.getDefender();
    Card attackingCard = game.getAttacker().getCardsOnField().get(attackingCardIndex);
    Card defendingCard = game.getDefender().getCardsOnField().get(defendingCardIndex);
    cardFight(attackingCard, defendingCard);
    if (isDead(attackingCard)){
      heroAttacker.getCardsOnField().remove(attackingCardIndex);
    }
    if (isDead(defendingCard)){
      heroDefender.getCardsOnField().remove(defendingCardIndex);
    }
  }
  private void cardFight(Card cardA, Card cardD){
    cardA.setHealth(cardA.getHealth() - cardD.getPower());
    cardD.setHealth(cardD.getHealth() - cardA.getPower());
  }

  private boolean isDead(Card card){
    return card.getHealth() <= 0;
  }

  public void heroAttack(int attackingCardIndex){
    Card attackingCard = game.getAttacker().getCardsOnField().get(attackingCardIndex - 1);
    Hero defender = game.getDefender();
    Hero attacker = game.getAttacker();
    defender.setHealth(defender.getHealth() - attackingCard.getPower());
    attacker.getCardsOnField().remove(attackingCard);
    attacker.getFreshCardsOnField().add(attackingCard);
  }

  public void putCard(int cardIndex, List<Card> availableCardsForPlay){
    Hero attacker = game.getAttacker();
    Card cardFromHand = availableCardsForPlay.get(cardIndex-1);
    attacker.getFreshCardsOnField().add(cardFromHand);
    attacker.setMana(attacker.getMana() - cardFromHand.getManaCost());
    attacker.getCardsOnHand().remove(cardFromHand);
  }

  private void makeCardsPlayble(){
    Hero attacker = game.getAttacker();
    List<Card> cardsOnField = attacker.getCardsOnField();
    List<Card> freshCardsOnField = attacker.getFreshCardsOnField();
    for (Card card : freshCardsOnField) {
      cardsOnField.add(card);
    }
    attacker.setFreshCardsOnField(new ArrayList<Card>());
  }

  public void finishTurn(){
    makeCardsPlayble();
    Hero attacker = game.getAttacker();
    Hero defender = game.getDefender();
    attacker.setMana((attacker.getCountTurn() + 1));
    attacker.setCountTurn(attacker.getCountTurn() + 1);
    attacker.setHisTurn(false);
    defender.setHisTurn(true);
    attacker.getCardsOnHand().add(randomizer.getRandomCard());
  }
}
