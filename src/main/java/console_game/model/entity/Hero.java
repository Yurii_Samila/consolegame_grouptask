package console_game.model.entity;

import java.util.ArrayList;
import java.util.List;

public class Hero {
  private int health;
  private int mana;
  private List<Card> cardsOnHand;
  private List<Card> cardsOnField;
  private List<Card> freshCardsOnField;
  private boolean hisTurn;
  private int countTurn = 1;
  private boolean isComputer;

  public boolean isComputer() {
    return isComputer;
  }

  public void setComputer(boolean computer) {
    isComputer = computer;
  }

  public int getCountTurn() {
    return countTurn;
  }

  public void setCountTurn(int countTurn) {
    this.countTurn = countTurn;
  }

  public Hero() {
    this.health = 5;
    this.mana = 1;
    this.cardsOnHand = new ArrayList<Card>();
    this.cardsOnField = new ArrayList<Card>();
    this.freshCardsOnField = new ArrayList<Card>();
  }

  public List<Card> getFreshCardsOnField() {
    return freshCardsOnField;
  }

  public void setFreshCardsOnField(List<Card> freshCardsOnField) {
    this.freshCardsOnField = freshCardsOnField;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  public int getMana() {
    return mana;
  }

  public void setMana(int mana) {
    this.mana = mana;
  }

  public boolean isHisTurn() {
    return hisTurn;
  }

  public void setHisTurn(boolean hisTurn) {
    this.hisTurn = hisTurn;
  }

  public List<Card> getCardsOnHand() {
    return cardsOnHand;
  }

  public void setCardsOnHand(List<Card> cardsOnHand) {
    this.cardsOnHand = cardsOnHand;
  }

  public List<Card> getCardsOnField() {
    return cardsOnField;
  }

  public void setCardsOnField(List<Card> cardsOnField) {
    this.cardsOnField = cardsOnField;
  }

  @Override
  public String toString() {
    return "Hero{" +
        "health=" + health +
        ", mana=" + mana +
        ", cardsOnHand=" + cardsOnHand +
        ", cardsOnField=" + cardsOnField +
        ", freshCardsOnField=" + freshCardsOnField +
        ", hisTurn=" + hisTurn +
        ", countTurn=" + countTurn +
        '}';
  }
}
