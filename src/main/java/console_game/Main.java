package console_game;

import console_game.controller.MainController;

public class Main {

  public static void main(String[] args) {
    MainController mainController = new MainController();
    mainController.startGame();
 }
}
