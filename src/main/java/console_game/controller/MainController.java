package console_game.controller;

import console_game.model.entity.Card;
import console_game.model.entity.Game;
import console_game.model.entity.Hero;
import console_game.model.entity.entityService.Randomizer;
import console_game.model.entity.entityService.Turn;
import console_game.view.GameView;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MainController {

  GameView gameView;
  Turn turn;
  Game game;
  Randomizer randomizer;
  final Scanner scn;
  boolean changeTurn = false;

  public MainController() {
    this.gameView = new GameView();
    this.game = new Game();
    this.turn = new Turn(this.game);
    this.randomizer = new Randomizer();
    scn = new Scanner(System.in);
  }

  public void startGame() {
    gameView.printWelcome();
    defineFirstTurn();
    gameView.chooseTheEnemy();
    setEnemy();
    while (isHeroStillAlive()) {
      System.out.println("New player");
      while (!this.changeTurn) {
        if (game.getAttacker().isComputer()) {
          System.out.println("Computer play");
          computerAction();
        } else {
          System.out.println("User game");
          gameView.printGameField(game);
          int userChoice = scn.nextInt();
          playerAction(userChoice);
        }
        System.out.println(game.getAttacker() + "  " + game.getAttacker().isComputer());
        System.out.println(game.getDefender() + "  " + game.getDefender().isComputer());
        if (!isHeroStillAlive()) {
          return;
        }
      }
      this.changeTurn = false;
    }
  }

  private void playerAction(int userChoice) {
    switch (userChoice) {
      case 1:
        putCardForUser();
        break;
      case 2:
        attackEnemyCardForUser();
        break;
      case 3:
        attackEnemyHeroForUser();
        break;
      case 4:
        this.changeTurn = true;
        turn.finishTurn();
        break;
      default:
        gameView.errorMessage();
        break;
    }
  }

  private void computerAction() {
    int computerChoice = numberOfComputerAction();
    switch (computerChoice) {
      case 1:
        System.out.println("Computer has put the card");
        putCardForComputer();
        break;
      case 2:
        System.out.println("Computer has attacked the card");
        attackEnemyCardForComputer();
        break;
      case 3:
        System.out.println("Computer has attacked the hero");
        attackEnemyHeroForComputer();
        break;
      case 4:
        System.out.println("Computer end turn");
        this.changeTurn = true;
        turn.finishTurn();
        break;
      default:
        gameView.errorMessage();
        break;
    }
  }

  private void putCardForComputer() {
    List<Card> availableCardsForPlay = availableCards();
    if (availableCardsForPlay.size() == 0) {
      gameView.noAvailableCardsWarning();
      return;
    }
    int computerChoice = randomizer.getRandomNum(availableCardsForPlay.size()) + 1;
    turn.putCard(computerChoice, availableCardsForPlay);
  }

  private void putCardForUser() {
    List<Card> availableCardsForPlay = availableCards();
    if (availableCardsForPlay.size() == 0) {
      gameView.noAvailableCardsWarning();
      return;
    }
    gameView.printCards(availableCardsForPlay);
    int playerChoice = scn.nextInt();
    turn.putCard(playerChoice, availableCardsForPlay);
  }

  private void attackEnemyCardForComputer() {
    List<Card> cardsOnAttackerField = game.getAttacker().getCardsOnField();
    List<Card> cardsOnDefenderField = game.getDefender().getCardsOnField();
    if (cardsOnAttackerField.size() == 0 || cardsOnDefenderField.size() == 0) {
      gameView.noAvailableCardsWarning();
      return;
    }
    int computerCard = randomizer.getRandomNum(cardsOnAttackerField.size());
    int enemyCard = randomizer.getRandomNum(cardsOnDefenderField.size());
    System.out.println(computerCard);
    System.out.println(enemyCard);
    turn.cardAttack(computerCard, enemyCard);
  }

  private void attackEnemyCardForUser() {
    List<Card> cardsOnAttackerField = game.getAttacker().getCardsOnField();
    List<Card> cardsOnDefenderField = game.getDefender().getCardsOnField();
    if (cardsOnAttackerField.size() == 0 || cardsOnDefenderField.size() == 0) {
      gameView.noAvailableCardsWarning();
      return;
    }
    gameView.printCards(cardsOnAttackerField);
    int playerCard = (scn.nextInt()) - 1;
    gameView.chooseEnemyCard();
    gameView.printCards(cardsOnDefenderField);
    int enemyCard = (scn.nextInt()) - 1;
    turn.cardAttack(playerCard, enemyCard);
  }

  private void attackEnemyHeroForComputer() {
    List<Card> cardsOnField = game.getAttacker().getCardsOnField();
    if (cardsOnField.size() == 0) {
      gameView.noAvailableCardsWarning();
      return;
    }
    int computerCard = randomizer.getRandomNum(cardsOnField.size()) + 1;
    turn.heroAttack(computerCard);
  }

  private void attackEnemyHeroForUser() {
    List<Card> cardsOnField = game.getAttacker().getCardsOnField();
    if (cardsOnField.size() == 0) {
      gameView.noAvailableCardsWarning();
      return;
    }
    gameView.printCards(cardsOnField);
    int playerCard = scn.nextInt();
    turn.heroAttack(playerCard);
  }
  private int numberOfComputerAction() {
    List<Integer> computerTurnVariants = defineNumberOfActions();
    if (computerTurnVariants.size() == 1) {
      return 4;
    } else {
      int computerChoice = randomizer.getRandomNum(computerTurnVariants.size());
      System.out.println("Computer choice " + computerChoice);
      return computerTurnVariants.get(computerChoice);
    }
  }

  private List<Integer> defineNumberOfActions() {
    List<Integer> computerTurnVariants = new ArrayList<>();
    System.out.println("avaliable cards list size " + availableCards().size());
    computerTurnVariants.add(4);
    if (availableCards().size() != 0) {
      computerTurnVariants.add(1);
      System.out.println("add in avaliableCards 1");
    }
    if (game.getAttacker().getCardsOnField().size() != 0) {
      System.out.println("add in avaliableCards 3");
      computerTurnVariants.add(3);
      if (game.getEnemy().getCardsOnField().size() != 0) {
        System.out.println("add in avaliableCards 2");
        computerTurnVariants.add(2);
      }
    }
    System.out.println(computerTurnVariants);
    return computerTurnVariants;
  }
  private void setEnemy() {
    int playerInput = this.scn.nextInt();
    if (playerInput == 1) {
      game.getEnemy().setComputer(true);
    }
  }

  private List<Card> availableCards() {
    final Hero attacker = game.getAttacker();
    List<Card> availableCardsForPlay = attacker
        .getCardsOnHand()
        .stream()
        .filter(o -> o.getManaCost() <= attacker.getMana())
        .collect(Collectors.toList());
    return availableCardsForPlay;
  }

  private void defineFirstTurn() {
    int randomNum = randomizer.getRandomNum(2);
    if (randomNum == 0) {
      game.getPlayer().setHisTurn(true);
    } else {
      game.getEnemy().setHisTurn(true);
    }
  }

  private boolean isHeroStillAlive() {
    if ((game.getAttacker().getHealth() <= 0) || (game.getEnemy().getHealth() <= 0)) {
      return false;
    }
    return true;
  }
}
